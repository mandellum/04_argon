﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] float speed = 20f;

    Quaternion startingRot;

    // Use this for initialization
    void Start()
    {
        startingRot = transform.rotation; // store initial position info
    }

    // Update is called once per frame
    void Update()
    {
                
        Vector3 rotateVector = new Vector3 (0f, Time.time * speed, 0f);
        transform.rotation = startingRot * Quaternion.Euler(rotateVector); // change transform.position
    }
}
